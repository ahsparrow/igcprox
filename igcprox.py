from datetime import timedelta

from igcparse import igcparse
import numpy as np
from scipy.interpolate import splrep, splev

EARTH_RADIUS = 6371000

# IGC GPS altitude is supposed to be referenced to the WGS84 ellipsoid.
# But quite a few loggers incorrectly use a geoid reference
GPS_GEOID_OFFSET = {
    'ClearNav Instruments, CNv-IGC': False,
    'EW-Avionics-microRecorder': True,
    'Flarm-IGC': False,
    'LX Eos': True,
    'LXN Mini Box Flarm': False,
    'LXN Red Box Flarm': False,
    'LXNAV,LX8000': True,
    'LXNAV,LX9000F': True,
    'LXNAV,LX9050F': True,
    'LXNAV,LX9070F': True,
    'LXNAV,S100': True,
    'LXNAV,NANO': True,
    'LXNAV,NANO3': True,
    'LXNAV,NANO4': True,
    'LXNAVIGATION,LX7007F': True,
    'LXNAVIGATION,COLIBRI': True,
    'LXNAVIGATION,LX_Colibri_II': True,
    'NaviterOudie-IGC': True,
    'PowerFLARM-IGC':  False
}

# Remove (incorrect) records with the same time
def unique(log):
    u, idx = np.unique(log['utc'], return_index=True)
    return log[idx]

# Get takeoff and landing indices
def flight_limits(data):
    dist = np.linalg.norm(data[:, 5:] - data[:, :-5], axis=0)

    land_idx = np.where(dist > 20)
    landing = land_idx[0][-1] - 60

    takeoff_idx = np.where(dist[:landing] > 10)
    try:
        takeoff = takeoff_idx[0][0] + 60
    except IndexError:
        takeoff = 60

    return takeoff, landing

# Get x/y/z samples with one second interpolation
def parse_igc_data(id, log):
    # Remove duplicate times for buggy Colibri loggers
    log = unique(log)

    utc, lat, lon, alt, alt_gps = log['utc'], log['lat'], log['lon'], log['alt'], log['alt_gps']

    # Check for >= 6s sampling
    delta_t = np.average(utc[1:] - utc[:-1])
    if delta_t > 6.01:
        print("%s sample interval > 6s, %.1f" % (id, delta_t))
        return None

    # Check for GPS drop-outs
    delta = np.max(np.abs(alt_gps[1:] - alt_gps[:-1]))
    if delta > 100:
        print("%s GPS drop-out, delta %d" % (id, delta))
        return None

    # Simple x/y conversion, valid for short distances
    x = lon * np.cos(lat) * EARTH_RADIUS
    y = lat * EARTH_RADIUS

    # Fuse pressure and GPS altitudes
    n = (int(60 // delta_t) // 2) * 2 + 1
    delta_alt = np.convolve(alt_gps - alt, np.ones((n,)) / n, mode='same')
    alt_fuse = alt + delta_alt

    #import matplotlib.pyplot as plt
    #plt.plot(alt, color='r')
    #plt.plot(alt_gps, color='g')
    #plt.plot(alt_fuse, color='b')
    #plt.plot(delta_alt)
    #plt.show()

    # Resample to one second interval using cubic splines
    t = np.arange(utc[0], utc[-1] + 1)

    x1 = splev(t, splrep(utc, x, s=0), der=0)
    y1 = splev(t, splrep(utc, y, s=0), der=0)
    z1 = splev(t, splrep(utc, alt_fuse, s=0), der=0)
    resampled = np.stack([x1, y1, z1])

    # Get takeoff and landing
    takeoff, landing = flight_limits(resampled)

    return {'begin': round(float(t[0])),
            'takeoff': takeoff,
            'landing': landing,
            'init_alt': alt_gps[0],
            'xyz': resampled}

# Get distance between to logs
def get_dist(log1, log2):
    takeoff1 = log1['begin'] + log1['takeoff']
    takeoff2 = log2['begin'] + log2['takeoff']

    # Get start and stop indices
    if takeoff1 > takeoff2:
        start1 = log1['takeoff']
        start2 = takeoff1 - log2['begin']
    else:
        start1 = takeoff2 - log1['begin']
        start2 = log2['takeoff']

    land1 = log1['begin'] + log1['landing']
    land2 = log2['begin'] + log2['landing']
    if land1 < land2:
        stop1 = log1['landing']
        stop2 = land1 - log2['begin']
    else:
        stop1 = land2 - log1['begin']
        stop2 = log2['landing']

    dist = np.linalg.norm(log1['xyz'][:, start1:stop1] - log2['xyz'][:, start2:stop2],
                          axis=0)

    return log1['begin'] + start1, dist

if __name__ == "__main__":
    import argparse
    import os
    import os.path

    import matplotlib.pyplot as plt

    parser = argparse.ArgumentParser()
    parser.add_argument("igc_dir", nargs="+")
    parser.add_argument("--ymax", type=int, help="fix Y-axis")
    parser.add_argument("--savefig", help="PDF file name")
    parser.add_argument("--showhits", help="glider reg|all")
    parser.add_argument("--threshold", default=30, type=int, help="distance, metres")
    parser.add_argument("--title", default="", help="plot title")
    parser.add_argument("--debug", action="store_true", help="print debug info")
    parser.add_argument("--geoid", type=int, default=46,
                        help="GPS Geoid offset, metres")

    args = parser.parse_args()

    hit_regs = {}
    for dir in args.igc_dir:
        if len(args.igc_dir) > 1:
            print("Directory %s" % dir)

        # Read all IGC logs from directory
        logs = {}
        for entry in os.scandir(dir):
            if (entry.is_file() and
                entry.name.endswith(".igc") and not entry.name.startswith(".")):
                id = entry.name.split('.')[0]
                if args.debug:
                    print("Reading %s" % id)

                fullpath = os.path.join(dir, entry.name)
                with open(fullpath, "r") as f:
                    igc = igcparse(f)

                    # GPS altitude correction
                    fty = igc['header'].get('fty')
                    if fty and fty in GPS_GEOID_OFFSET:
                        if GPS_GEOID_OFFSET[fty]:
                            igc['data']['alt_gps'] += args.geoid
                    else:
                        print("%s unknown logger, %s" % (id, fty))
                        continue

                    data = parse_igc_data(id, igc['data'])
                    if data is None:
                      continue

                    logs[id] = data

        if args.debug:
            print("Takeoff/landing")
            print("---------------")

            for id in logs:
                print(id, logs[id]['takeoff'], logs[id]['landing'])

        # Sanity check intitial altitudes
        median_alt = np.median([a['init_alt'] for a in logs.values()])
        for id in list(logs):
            offset = abs(logs[id]['init_alt'] - median_alt)
            if offset > 15:
                print("%s altitude offset > 15m, %d" % (id, offset))
                logs.pop(id)

        # Get close encounters
        for id1 in sorted(logs):
            for id2 in sorted(logs):
                if id1 != id2:
                  tim, dist = get_dist(logs[id1], logs[id2])
                  hits = np.where(dist < args.threshold)

                  hit_regs[id1] = hit_regs.get(id1, []) + [id2] * len(hits[0])

                  if ((args.showhits == "all") or (args.showhits == id1)) and len(hits[0]) != 0:
                      print("---------")
                      print("%s %s" % (id1, id2))
                      print([str(timedelta(seconds=int(i + tim))) for i in hits[0]])
                      print(dist[hits])

    # Draw the plot
    regs = sorted(hit_regs.keys())

    counts = [len(hit_regs[reg]) for reg in regs]
    x_pos = np.arange(len(regs))

    fig, ax = plt.subplots()
    ax.bar(x_pos, counts, label="X")
    ax.set_xticks(x_pos)
    ax.set_xticklabels(regs, rotation=90)
    ax.set_title("%s Threshold %i m" % (args.title, args.threshold))
    ax.set_ylabel("Seconds")
    if args.ymax:
        ax.set_ylim(0, args.ymax)

    #fig.text(0.5, 0.5, "Experimental",
    #         fontsize=50, color="gray", alpha=0.5,
    #         horizontalalignment='center', verticalalignment='center',
    #         rotation=45)

    rects = ax.patches

    # Now make some labels - number of conflicting gliders
    labels = ["%d" % len(set(i)) if len(i) > 0 else ""
              for i in [hit_regs[reg] for reg in regs]]

    for rect, label in zip(rects, labels):
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2, height, label, ha='center', va='bottom')

    if args.savefig:
        fig.savefig(args.savefig, dpi=200)

    plt.show()
