from urllib.parse import urlparse, urlunsplit
from urllib.request import urlopen

from bs4 import BeautifulSoup

def scrape(url):
    split = urlparse(url)

    html = urlopen(url)
    soup = BeautifulSoup(html, "lxml")

    for link in soup.find_all('a'):
        href = link.get('href')

        if href and href.find('download-contest-flight') != -1:
            d_url = urlunsplit((split.scheme, split.netloc, href, "", ""))

            with urlopen(d_url) as igcdata:
                print(link.string)
                with open(link.string + ".igc", "wb") as f:
                    f.write(igcdata.read())

if __name__ == "__main__":
  from argparse import ArgumentParser

  parser = ArgumentParser()
  parser.add_argument("url")

  args = parser.parse_args()
  scrape(args.url)
